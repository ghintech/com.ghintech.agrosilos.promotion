package com.ghintech.agrosilos.promotion.component;

import org.adempiere.base.IModelValidatorFactory;
import org.compiere.model.ModelValidator;

import com.ghintech.agrosilos.promotion.modelvalidator.PromotionValidator;

public class PromotionModelvalidatorFactory implements IModelValidatorFactory{

	@Override
	public ModelValidator newModelValidatorInstance(String className) {
		// TODO Auto-generated method stub
		if(className.equals("com.ghintech.agrosilos.promotion.modelvalidator.PromotionValidator"))
			return new PromotionValidator();
		return null;
	}

}
