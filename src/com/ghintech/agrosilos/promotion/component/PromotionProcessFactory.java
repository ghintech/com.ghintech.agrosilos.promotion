package com.ghintech.agrosilos.promotion.component;

import org.adempiere.base.IProcessFactory;
import org.compiere.process.ProcessCall;

import com.ghintech.agrosilos.promotion.process.GenerateDistributionAndRewardProm;

public class PromotionProcessFactory implements IProcessFactory{

	@Override
	public ProcessCall newProcessInstance(String className) {
		// TODO Auto-generated method stub
		if(className.equals("com.ghintech.agrosilos.promotion.process.GenerateDistributionAndRewardProm"))
			return new GenerateDistributionAndRewardProm();
			
		return null;
	}

}
