package com.ghintech.agrosilos.promotion.process;

import java.math.BigDecimal;
import java.util.logging.Level;

import org.adempiere.model.MPromotionDistribution;
import org.adempiere.model.MPromotionReward;
import org.compiere.model.Query;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;


public class GenerateDistributionAndRewardProm extends SvrProcess {

	protected int p_M_PromotionDistribution_ID;
	
	private BigDecimal DistSumValue;
	private BigDecimal DistTopValue;
	private BigDecimal RewardSumValue;
	@Override
	protected void prepare() {
		
		// TODO Auto-generated method stub
		for (ProcessInfoParameter para : this.getParameter()) {
            String name = para.getParameterName();
            if (para.getParameter() == null) continue;
            
            if (name.equals("DistSumValue")) {
                this.DistSumValue = (BigDecimal)para.getParameter();
                continue;
            }
            if (name.equals("DistTopValue")) {
                this.DistTopValue = (BigDecimal)para.getParameter();
                continue;
            }
            if (name.equals("RewardSumValue")) {
                this.RewardSumValue = (BigDecimal)para.getParameter();
                continue;
            }
            
            
            this.log.log(Level.SEVERE, "Unknown Parameter: " + name);
        }
		p_M_PromotionDistribution_ID = getRecord_ID();
	}

	@Override
	protected String doIt() throws Exception {
		// TODO Auto-generated method stub
		MPromotionDistribution pdfrom = new MPromotionDistribution(getCtx(), p_M_PromotionDistribution_ID, get_TrxName());
		MPromotionReward prfrom = new Query(getCtx(),MPromotionReward.Table_Name,"M_PromotionDistribution_ID=?",get_TrxName())
		.setParameters(pdfrom.get_ID())
		.first();
		BigDecimal QtyRewardNew=prfrom.getQty().add(RewardSumValue);
		
		BigDecimal QtyDistributionNew=pdfrom.getQty().add(DistSumValue);
		int i=1;
		int SeqDistributionNo=pdfrom.getSeqNo();
		int SeqRewardNo=prfrom.getSeqNo();
		while(QtyDistributionNew.compareTo(DistTopValue)<=0){
			//distribution
			MPromotionDistribution pdto=new MPromotionDistribution(getCtx(),0, get_TrxName());
			MPromotionDistribution.copyValues(pdfrom, pdto);
			pdto.setQty(QtyDistributionNew);
			pdto.setSeqNo(SeqDistributionNo-=10);
			pdto.saveEx();
			
			QtyDistributionNew=QtyDistributionNew.add(DistSumValue);
			
			//reward
			MPromotionReward prto = new MPromotionReward(getCtx(), 0, get_TrxName());
			MPromotionReward.copyValues(prfrom, prto);
			prto.setM_PromotionDistribution_ID(pdto.get_ID());
			prto.setM_TargetDistribution_ID(pdto.get_ID());
			prto.setQty(QtyRewardNew);
			prto.setSeqNo(SeqRewardNo-=10);
			prto.saveEx();
			
			QtyRewardNew=QtyRewardNew.add(RewardSumValue);
			
			
			i++;
		}
		return "Cantidad de lienas creadas: "+i;
	}


}
